chevy        = Make.create(name: "Chevrolet")
chevy_models = ["Malibu", "Impala", "Camaro", "Tahoe"]
chevy_models.each do |model|
  Model.create(name: model, make: chevy)
  puts "Created Chevrolet #{model}"
end

honda        = Make.create(name: "Honda")
honda_models = ["Accord", "Civic", "Odyssey"]
honda_models.each do |model|
  Model.create(name: model, make: honda)
  puts "Created Honda #{model}"
end


subaru        = Make.create(name: "Subaru")
subaru_models = ["Legacy", "Forester"]
subaru_models.each do |model|
  Model.create(name: model, make: subaru)
  puts "Created Subaru #{model}"
end

toyota        = Make.create(name: "Toyta")
toyota_models = ["Camry", "Sienna", "Prius"]
toyota_models.each do |model|
  Model.create(name: model, make: toyota)
  puts "Created Toyota #{model}"
end

options = ["Voice control", "Heated seats", "Backup Camera"]
options.each do |option|
  Option.create(name: option)
end

style_map = {
  "Malibu" => "Sedan",
  "Impala" => "Sedan",
  "Camaro" => "Sedan",
  "Tahoe" => "SUV",
  "Accord" => "Sedan",
  "Civic" => "Sedan",
  "Odyssey" => "Minivan",
  "Legacy" => "Sedan",
  "Forester" => "SUV",
  "Camry" => "Sedan",
  "Sienna" => "Minivan",
  "Prius" => "Sedan"
}

Model.all.each do |model|
  Vehicle.create(
    model:   model,
    year:    rand(1999..2018),
    mileage: rand(0..100000),
    style:   style_map[model.name],
    color:   Vehicle::COLORS.sample
  ).vehicle_options.create(option: Option.order("RANDOM()").first)
end

