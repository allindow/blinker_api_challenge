class CreateVehicleOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :vehicle_options do |t|
      t.references :vehicle, foreign_key: true, null: false
      t.references :option, foreign_key: true, null: false
    end
  end
end
