class CreateModels < ActiveRecord::Migration[5.0]
  def change
    create_table :models do |t|
      t.references :make, foreign_key: true
      t.string :name, null: false
      t.timestamps
    end
  end
end
