class CreateVehicles < ActiveRecord::Migration[5.0]
  def change
    create_table :vehicles do |t|
      t.references :model, foreign_key: true, null: false
      t.string :year, null: false
      t.string :mileage, null: false
      t.string :style, null: false
      t.string :color, null: false
    end
  end
end
