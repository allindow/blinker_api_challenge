require 'rails_helper'

RSpec.describe "Options", type: :request do
  it "should get all options" do
    2.times { create(:option) }

    get '/api/v1/options'

    expect(response).to be_success
    expect(json(response).first["name"]).to eq(Option.first.name)
    expect(json(response).last["name"]).to eq(Option.last.name)
  end

  it "should create an option" do
    post '/api/v1/options?name=Voice+control'

    expect(Option.count).to eq(1)
    expect(Option.last.name).to eq("Voice control")
    expect(json(response)["name"]).to eq("Voice control")
  end

  it "responds with status 400 if validations fail" do
    post '/api/v1/options?name=Voice+control'

    expect(Option.count).to eq(1)
    expect(Option.last.name).to eq("Voice control")
    expect(json(response)["name"]).to eq("Voice control")

    post '/api/v1/options?name=Voice+control'

    expect(Option.count).to eq(1)
    expect(response.status).to eq(400)
    expect(response.message).to eq("Bad Request")
    expect(json(response)["message"]).to eq("Name has already been taken")
  end

  it "should edit an option" do
    option = create(:option, name: "Subaru")

    put "/api/v1/options/#{option.id}?name=Honda"

    option.reload
    expect(option.name).to eq("Honda")
    expect(json(response)["name"]).to eq("Honda")
  end

  it "should delete an option" do
    option = create(:option)

    delete "/api/v1/options/#{option.id}"

    expect(Option.count).to eq(0)
    expect(response.status).to eq(204)
    expect(response.message).to eq("No Content")
  end

  def json(response)
    JSON.parse(response.body)
  end
end