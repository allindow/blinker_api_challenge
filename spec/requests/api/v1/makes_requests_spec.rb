require 'rails_helper'

RSpec.describe "Makes", type: :request do
  it "should get all makes" do
    2.times { create(:make) }

    get '/api/v1/makes'

    expect(response).to be_success
    expect(json(response).first["name"]).to eq(Make.first.name)
    expect(json(response).last["name"]).to eq(Make.last.name)
  end

  it "should create a make" do
    post '/api/v1/makes?name=Honda'

    expect(Make.count).to eq(1)
    expect(Make.last.name).to eq("Honda")
  end

  it "returns status 400 if validations fail" do
    post "/api/v1/makes"

    expect(response.status).to eq(400)
    expect(response.message).to eq("Bad Request")
    expect(json(response)["message"]).to eq("Name can't be blank")
  end

  it "should edit a make" do
    make = create(:make, name: "Subaru")

    put "/api/v1/makes/#{make.id}?name=Honda"

    make.reload
    expect(make.name).to eq("Honda")
    expect(json(response)["name"]).to eq("Honda")
  end

  it "should delete a make" do
    make = create(:make)

    delete "/api/v1/makes/#{make.id}"

    expect(Make.count).to eq(0)
    expect(response.status).to eq(204)
    expect(response.message).to eq("No Content")
  end

  def json(response)
    JSON.parse(response.body)
  end
end