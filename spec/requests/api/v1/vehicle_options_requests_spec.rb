require 'rails_helper'

RSpec.describe "Vehicle Options", type: :request do
  it "should associate options with a vehicle" do
    option1 = create(:option)
    option2 = create(:option)
    vehicle = create(:vehicle)

    post "/api/v1/vehicle_options?vehicle_id=#{vehicle.id}&option_id=#{option1.id}"
    post "/api/v1/vehicle_options?vehicle_id=#{vehicle.id}&option_id=#{option2.id}"

    expect(vehicle.options.first).to eq(option1)
    expect(vehicle.options.last).to eq(option2)
    expect(json(response)["id"]).to eq(vehicle.id)
    expect(json(response)["options"].first["name"]).to eq(option1.name)
    expect(json(response)["options"].last["name"]).to eq(option2.name)
  end

  def json(response)
    JSON.parse(response.body)
  end
end