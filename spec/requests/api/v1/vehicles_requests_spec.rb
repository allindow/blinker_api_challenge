require 'rails_helper'

RSpec.describe "Vehicles", type: :request do
  it "should get all vehicles" do
    2.times { create(:vehicle) }

    get '/api/v1/vehicles'

    expect(response).to be_success

    expect(json(response).first["make"]).to eq(Vehicle.first.model.make.name)
    expect(json(response).first["model"]).to eq(Vehicle.first.model.name)
    expect(json(response).first["year"]).to eq(Vehicle.first.year)
    expect(json(response).first["mileage"]).to eq(Vehicle.first.mileage)
    expect(json(response).first["style"]).to eq(Vehicle.first.style)
    expect(json(response).first["color"]).to eq(Vehicle.first.color)

    expect(json(response).last["make"]).to eq(Vehicle.last.model.make.name)
    expect(json(response).last["model"]).to eq(Vehicle.last.model.name)
    expect(json(response).last["year"]).to eq(Vehicle.last.year)
    expect(json(response).last["mileage"]).to eq(Vehicle.last.mileage)
    expect(json(response).last["style"]).to eq(Vehicle.last.style)
    expect(json(response).last["color"]).to eq(Vehicle.last.color)
  end

  it "should create a vehicle" do
    model = create(:model)
    post "/api/v1/vehicles?model_id=#{model.id}&year=2017&mileage=0&style=Sedan&color=Red"

    expect(Vehicle.count).to eq(1)
    expect(json(response)["make"]).to eq(model.make.name)
    expect(json(response)["model"]).to eq(model.name)
    expect(json(response)["year"]).to eq("2017")
    expect(json(response)["mileage"]).to eq("0")
    expect(json(response)["style"]).to eq("Sedan")
    expect(json(response)["color"]).to eq("Red")
  end

  it "returns status 400 if validations fail" do
    model = create(:model)

    post "/api/v1/vehicles?model_id=#{model.id}&year=2017&mileage=0&style=Hatchback&color=Purple"

    expect(response.status).to eq(400)
    expect(json(response)["message"]).to eq("Style Hatchback is not an option, Color Purple is not an option")
  end

  it "should edit a vehicle" do
    vehicle = create(:vehicle, color: "Red")

    put "/api/v1/vehicles/#{vehicle.id}?color=Black"

    vehicle.reload
    expect(vehicle.color).to eq("Black")
    expect(json(response)["color"]).to eq("Black")
  end

  it "should delete a vehicle" do
    vehicle = create(:vehicle)

    delete "/api/v1/vehicles/#{vehicle.id}"

    expect(Vehicle.count).to eq(0)
    expect(response.status).to eq(204)
    expect(response.message).to eq("No Content")
  end

  def json(response)
    JSON.parse(response.body)
  end
end