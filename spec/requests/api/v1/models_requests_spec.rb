require 'rails_helper'

RSpec.describe "Models", type: :request do
  it "should get all models" do
    2.times { create(:model) }

    get '/api/v1/models'

    expect(response).to be_success
    expect(json(response).first["name"]).to eq(Model.first.name)
    expect(json(response).first["make"]).to eq(Model.first.make.name)
    expect(json(response).last["name"]).to eq(Model.last.name)
    expect(json(response).last["make"]).to eq(Model.last.make.name)
  end

  it "should create a model" do
    post '/api/v1/models?name=Outback'

    expect(Model.count).to eq(1)
    expect(Model.last.name).to eq("Outback")
    expect(json(response)["name"]).to eq("Outback")
  end

  it "can be associated with a make" do
    make = create(:make, name: "Honda")

    post "/api/v1/models?name=Accord&make_id=#{make.id}"

    expect(Model.count).to eq(1)
    expect(Model.last.make).to eq(make)
    expect(json(response)["name"]).to eq("Accord")
    expect(json(response)["make"]).to eq("Honda")
  end

  it "returns status 400 if validations fail" do
    make = create(:make, name: "Honda")

    post "/api/v1/models?name=Accord&make_id=#{make.id}"

    expect(Model.count).to eq(1)
    expect(response.status).to eq(200)

    post "/api/v1/models?name=Accord&make_id=#{make.id}"

    expect(Model.count).to eq(1)
    expect(response.status).to eq(400)
    expect(response.message).to eq("Bad Request")
    expect(json(response)["message"]).to eq("Name has already been taken")
  end

  it "should edit a model" do
    model = create(:model, name: "Civic")

    put "/api/v1/models/#{model.id}?name=Accord"

    model.reload
    expect(model.name).to eq("Accord")
    expect(json(response)["name"]).to eq("Accord")
  end

  it "should delete a model" do
    model = create(:model)

    delete "/api/v1/models/#{model.id}"

    expect(Model.count).to eq(0)
    expect(response.status).to eq(204)
    expect(response.message).to eq("No Content")
  end

  def json(response)
    JSON.parse(response.body)
  end
end