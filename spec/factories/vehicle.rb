FactoryBot.define do
  factory :vehicle do
    association :model, factory: :model
    year {rand(1999..2018)}
    mileage {rand(0..100000)}
    style Vehicle::STYLES.sample
    color Vehicle::COLORS.sample
  end
end