FactoryBot.define do
  factory :make do
    sequence :name do |n|
      "Make #{n}"
    end
  end
end