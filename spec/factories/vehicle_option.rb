FactoryBot.define do
  factory :vehicle_option do
    association :vehicle, factory: :vehicle
    association :option, factory: :option
  end
end