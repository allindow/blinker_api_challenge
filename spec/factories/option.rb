FactoryBot.define do
  factory :option do
    sequence :name do |n|
      "Option #{n}"
    end
  end
end