FactoryBot.define do
  factory :model do
    sequence :name do |n|
      "Model #{n}"
    end
    association :make, factory: :make
  end
end