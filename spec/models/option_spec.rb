require 'rails_helper'

RSpec.describe Option, type: :model do
  it { should validate_uniqueness_of(:name) }
  it { should have_many(:vehicles) }

  subject { create(:option) }
  it { should validate_uniqueness_of(:name) }
end