require 'rails_helper'

RSpec.describe Make, type: :model do
  it { should have_many(:models) }
  it { should validate_presence_of(:name) }
end