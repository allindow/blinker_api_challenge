require 'rails_helper'

RSpec.describe Model, type: :model do
  it { should belong_to(:make) }
  it { should have_many(:vehicles) }
  it { should validate_presence_of(:name) }
  it "should validate uniqueness of name scoped to make" do
    make = create(:make)
    create(:model, make_id: make.id)
    should validate_uniqueness_of(:name).scoped_to(:make_id)
  end

  it "validates that the make exists for an association" do
    model = Model.create(name: "Civic", make_id: 1)
    expect(model).to be_invalid
    expect(model.errors.messages[:make]).to include("not found")
  end
end