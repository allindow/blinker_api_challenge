require 'rails_helper'

RSpec.describe Vehicle, type: :model do
  it { should belong_to(:model) }
  it { should have_many(:options) }

  it "validates that style and color are available options" do
    vehicle = Vehicle.create(
      model:   create(:model),
      year:    rand(1999..2018),
      mileage: rand(0..100000),
      style:   "Hatchback",
      color:   "Purple"
    )
    expect(vehicle).to be_invalid
    expect(vehicle.errors.messages[:color]).to include("Purple is not an option")
    expect(vehicle.errors.messages[:style]).to include("Hatchback is not an option")
  end
end