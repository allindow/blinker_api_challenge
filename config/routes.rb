Rails.application.routes.draw do
  namespace :api, defaults: { format: JSON } do
    namespace :v1 do
      resources :makes, except: [:new, :edit]
      resources :models, except: [:new, :edit]
      resources :options, except: [:new, :edit]
      resources :vehicles, except: [:new, :edit]
      resources :vehicle_options, only: [:create]
    end
  end
end