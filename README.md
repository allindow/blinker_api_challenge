# Blinker API Challenge

The purpose of this challenge was to create a RESTful Vehicle API. 
After migrating your database, run `rails db:seed` to set up some data for acccessing the endpoints.

# Makes
**List all Makes**
----

* **URL**

  /api/v1/makes

* **Method:**

  `GET`
  
*  **No Params**

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[{"name":"Chevrolet","id":1},{"name":"Honda","id":2},{"name":"Subaru","id":3},{"name":"Toyta","id":4}]`
 
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/makes",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Create a Make**
----

* **URL**

  /api/v1/makes

* **Method:**

  `POST`
  
*  **URL Params**
    * None

* **Data Params**
    * **Required:**
        * `name=[make_name]`<br>
        example: name=Chevrolet


* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"name":"Chevrolet","id":1}`
    
* **Error Response:**

  * **Code:** 400 Bad Request <br />
    **Content:**
    * `{"status":"error","code":400,"message":"Name has already been taken"}`
    * `{"status":"error","code":400,"message":"Name can't be blank"}`
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/makes?name=Chevrolet",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Edit a Make**
----

* **URL**

  /api/v1/makes/:id

* **Method:**

  `PUT`
  
*  **URL Params**
    * **Required:**
        * `id=[integer]`

* **Data Params**
    * **Required:**
        * `name=[make_name]`<br>
        example: name=Chevrolet


* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"name":"Chevrolet","id":1}`
    
* **Error Response:**
  * **Code:** 404 Not Found <br />
  * **Code:** 400 Bad Request <br />
    **Content:**
    * `{"status":"error","code":400,"message":"Name has already been taken"}`
    * `{"status":"error","code":400,"message":"Name can't be blank"}`
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/makes/1?name=Chevrolet",
      dataType: "json",
      type : "PUT",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Delete a Make**
----

* **URL**

  /api/v1/makes/:id

* **Method:**

  `DELETE`
  
*  **URL Params**
    * **Required:**
        * `id=[integer]`

* **Data Params**
    * None


* **Success Response:**

  * **Code:** 204 <br />
    
* **Error Response:**
  * **Code:** 404 Not Found <br />
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/makes/1",
      dataType: "json",
      type : "DELETE",
      success : function(r) {
        console.log(r);
      }
    });
  ```
  
# Models
**List all Models**
----

* **URL**

  /api/v1/models

* **Method:**

  `GET`
  
*  **No Params**

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[{"name":"Malibu","make":"Chevrolet","id":1},{"name":"Impala","make":"Chevrolet","id":2}]`
 
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/models",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Create a Model**
----

* **URL**

  /api/v1/models

* **Method:**

  `POST`
  
*  **URL Params**
    * None

* **Data Params**
    * **Required:**
        * `name=[model_name]`<br>
     example: name=Impala
  
    * **Optional to associate model with a make:**
        * `make_id=[integer]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"name":"Impala", "make":"Chevrolet", id":1}`
    
* **Error Response:**

  * **Code:** 400 Bad Request <br />
    **Content:**
    * `{"status":"error","code":400,"message":"Name has already been taken"}`
    * `{"status":"error","code":400,"message":"Make not found"}`
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/models?name=Impala&make_id=1",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Edit a Model**
----

* **URL**

  /api/v1/models/:id

* **Method:**

  `PUT`
  
*  **URL Params**

    * **Required:**
   
        * `id=[integer]`

* **Data Params**

    * **Required:**
     
        * `name=[model_name]`<br>
       example: name=Impala
    
    * **Optional to associate model with a make:**
       
        * `make_id=[integer]`<br>

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"name":"Impala", "make":"Chevrolet", id":1}`
    
* **Error Response:**
  * **Code:** 404 Not Found <br />
  * **Code:** 400 Bad Request <br />
    **Content:**
    * `{"status":"error","code":400,"message":"Name has already been taken"}`
    * `{"status":"error","code":400,"message":"Make not found"}`
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/models/1?name=Impala",
      dataType: "json",
      type : "PUT",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Delete a Model**
----

* **URL**

  /api/v1/models/:id

* **Method:**

  `DELETE`
  
*  **URL Params**
    * **Required:**
        * `id=[integer]`

* **Data Params**
    * None


* **Success Response:**

  * **Code:** 204 <br />
    
* **Error Response:**
  * **Code:** 404 Not Found <br />
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/models/1",
      dataType: "json",
      type : "DELETE",
      success : function(r) {
        console.log(r);
      }
    });
  ```
# Options
**List all Options**
----

* **URL**

  /api/v1/options

* **Method:**

  `GET`
  
*  **No Params**

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[{"name":"Voice control","id":1},{"name":"Heated seats","id":2},{"name":"Backup Camera","id":3}]`
 
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/options",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Create an Option**
----

* **URL**

  /api/v1/options

* **Method:**

  `POST`
  
*  **URL Params**
    * None

* **Data Params**

    * **Required:**
        * `name=[option_name]`<br>
     example: name=Voice+control

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"name":"Voice control", id":1}`
    
* **Error Response:**

  * **Code:** 400 Bad Request <br />
    **Content:**
    * `{"status":"error","code":400,"message":"Name has already been taken"}`
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/options?name=Voice+control",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Edit an Option**
----

* **URL**

  /api/v1/options/:id

* **Method:**

  `PUT`
  
*  **URL Params**

    * **Required:**
        * `id=[integer]`

* **Data Params**
    * **Required:**
       * `name=[option_name]`<br>
       example: name=Voice+control

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"name":"Voice control", id":1}`
    
* **Error Response:**
  * **Code:** 404 Not Found <br />
  * **Code:** 400 Bad Request <br />
    **Content:**
    * `{"status":"error","code":400,"message":"Name has already been taken"}`
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/options/1?name=Voice+control",
      dataType: "json",
      type : "PUT",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Delete an Option**
----

* **URL**

  /api/v1/options/:id

* **Method:**

  `DELETE`
  
*  **URL Params**
    * **Required:**
        * `id=[integer]`

* **Data Params**

  None


* **Success Response:**

  * **Code:** 204 <br />
    
* **Error Response:**
  * **Code:** 404 Not Found <br />
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/options/1",
      dataType: "json",
      type : "DELETE",
      success : function(r) {
        console.log(r);
      }
    });
  ```
  
# Vehicles
**List all Vehicles**
----

* **URL**

  /api/v1/vehicles

* **Method:**

  `GET`
  
*  **No Params**

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `[{"make":"Chevrolet","model":"Malibu","year":"2016","mileage":"95974","style":"Sedan","color":"Yellow","id":1,"options":[{"name":"Backup Camera"}]},{"make":"Chevrolet","model":"Impala","year":"2018","mileage":"89266","style":"Sedan","color":"Yellow","id":2,"options":[{"name":"Voice control"}]}]`
 
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/vehicles",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Create a Vehicle**
----

* **URL**

  /api/v1/vehicles

* **Method:**

  `POST`
  
*  **URL Params**
    * None

* **Data Params**
    * **Required:**
        * `year=[intger]`<br>
        * `mileage=[integer]`<br>
        * `model_id=[integer]`<br>
        * `style=[style_name]`<br>
       Style must be one of the following: "Sedan", "Minivan", "SUV", "Pickup"<br>
       example: style=Sedan
        * `color=[color_name]`<br>
       Color must be one of the following: "Red", "Blue", "Yellow", "Green", "Black", "Silver", "White"
       example: color=Red

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"make":"Chevrolet","model":"Malibu","year":"2016","mileage":"95974","style":"Sedan","color":"Yellow","id":1,"options":[]}`
    
* **Error Response:**

  * **Code:** 400 Bad Request <br />
    **Content:**
    * `{"status":"error","code":400,"message":"Model must exist, Model not found"}`
    * `{"status":"error","code":400,"message":"Style Hatchback is not an option"}`
    * `{"status":"error","code":400,"message":"Color Purple is not an option"}`
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/vehicles?model_id=1&year=2017&mileage=0&style=Sedan&color=Red",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Edit a Vehicle**
----

* **URL**

  /api/v1/vehicles/:id

* **Method:**

  `PUT`
  
*  **URL Params**
    * **Required:**
        * `id=[integer]`

* **Data Params**
    * **Optional:**
        * `year=[intger]`<br>
        * `mileage=[integer]`<br>
        * `model_id=[integer]`<br>
        * `style=[style_name]`<br>
       Style must be one of the following: "Sedan", "Minivan", "SUV", "Pickup"<br>
       example: style=Sedan
        * `color=[color_name]`<br>
       Color must be one of the following: "Red", "Blue", "Yellow", "Green", "Black", "Silver", "White"
       example: color=Red

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"make":"Chevrolet","model":"Malibu","year":"2016","mileage":"95974","style":"Sedan","color":"Yellow","id":1,"options":[]}`
    
* **Error Response:**
  * **Code:** 400 Bad Request <br />
    **Content:**
    * `{"status":"error","code":400,"message":"Model must exist, Model not found"}`
    * `{"status":"error","code":400,"message":"Style Hatchback is not an option"}`
    * `{"status":"error","code":400,"message":"Color Purple is not an option"}`
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/vehicles/1?model_id=1&year=2017&mileage=0&style=Sedan&color=Red",
      dataType: "json",
      type : "PUT",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Delete a Vehicle**
----

* **URL**

  /api/v1/vehicles/:id

* **Method:**

  `DELETE`
  
*  **URL Params**
    * **Required:**
        * `id=[integer]`

* **Data Params**
    * None


* **Success Response:**

  * **Code:** 204 <br />
    
* **Error Response:**
  * **Code:** 404 Not Found <br />
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/vehicles/1",
      dataType: "json",
      type : "DELETE",
      success : function(r) {
        console.log(r);
      }
    });
```

# Associate Options to Vehicle

**Create Association**
----

* **URL**

  /api/v1/vehicle_options

* **Method:**

  `POST`
  
*  **URL Params**
    * None

* **Data Params**
    * **Required:**
        * `vehicle_id=[intger]`<br>
        * `option_id=[integer]`<br>

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"make":"Chevrolet","model":"Malibu","year":"2017","mileage":"95974","style":"Sedan","color":"Yellow","id":1,"options":[{"name":"Backup Camera"}]}`
    
* **Error Response:**

  * **Code:** 400 Bad Request <br />
    **Content:**
    * `{"status":"error","code":400,"message":"Option must exist, Option can't be blank"}`
    * `{"status":"error","code":400,"message":"Vehicle must exist, Vehicle can't be blank"}`
    * `{"status":"error","code":400,"message":"Vehicle has already been taken"}`
    
* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/v1/vehicle_options?vehicle_id=1&option_id=1",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```
  