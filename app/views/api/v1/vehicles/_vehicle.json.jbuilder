json.make vehicle.model.make.name
json.model vehicle.model.name
json.year vehicle.year
json.mileage vehicle.mileage
json.style vehicle.style
json.color vehicle.color
json.id vehicle.id
json.options vehicle.options do |option|
  json.name option.try(:name)
end