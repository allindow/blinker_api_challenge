class Api::V1::MakesController < ApplicationController
  def index
    @makes = Make.all
    render 'api/v1/makes/index'
  end

  def create
    @make = Make.create(make_params)
    if @make.valid?
      render 'api/v1/makes/show'
    else
      handle_errors(@make)
    end
  end

  def update
    @make = Make.find(params[:id])
    if @make.update(make_params)
      render 'api/v1/makes/show'
    else
      handle_errors(@make)
    end
  end

  def destroy
    head 204 if Make.delete(params[:id])
  end

  private

  def make_params
    params.permit(:name)
  end
end
