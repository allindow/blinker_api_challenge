class Api::V1::VehicleOptionsController < ApplicationController
  def create
    @vehicle_option = VehicleOption.create(vehicle_option_params)
    if @vehicle_option.valid?
      @vehicle = Vehicle.find(params[:vehicle_id])
      render 'api/v1/vehicles/show'
    else
      handle_errors(@vehicle_option)
    end
  end

  private

  def vehicle_option_params
    params.permit(:vehicle_id, :option_id)
  end
end