class Api::V1::OptionsController < ApplicationController
  def index
    @options = Option.all
    render 'api/v1/options/index'
  end

  def create
    @option = Option.create(option_params)
    if @option.valid?
      render 'api/v1/options/show'
    else
      handle_errors(@option)
    end
  end

  def update
    @option = Option.find(params[:id])
    if @option.update(option_params)
      render 'api/v1/options/show'
    else
      handle_errors(@option)
    end
  end

  def destroy
    head 204 if Option.delete(params[:id])
  end

  private

  def option_params
    params.permit(:name)
  end
end
