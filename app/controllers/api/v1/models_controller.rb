class Api::V1::ModelsController < ApplicationController
  def index
    @models = Model.all
    render 'api/v1/models/index'
  end

  def create
    @model = Model.create(model_params)
    if @model.valid?
      render 'api/v1/models/show'
    else
      handle_errors(@model)
    end
  end

  def update
    @model = Model.find(params[:id])
    if @model.update(model_params)
      render 'api/v1/models/show'
    else
      handle_errors(@model)
    end
  end

  def destroy
    head 204 if Model.delete(params[:id])
  end

  private

  def model_params
    params.permit(:name, :make_id)
  end
end
