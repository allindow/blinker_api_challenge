class Api::V1::VehiclesController < ApplicationController
  def index
    @vehicles = Vehicle.all
    render 'api/v1/vehicles/index'
  end

  def create
    @vehicle = Vehicle.create(vehicle_params)
    if @vehicle.valid?
      render 'api/v1/vehicles/show'
    else
     handle_errors(@vehicle)
    end
  end

  def update
    @vehicle = Vehicle.find(params[:id])
    if @vehicle.update(vehicle_params)
      render 'api/v1/vehicles/show'
    else
      handle_errors(@vehicle)
    end
  end

  def destroy
    head 204 if Vehicle.delete(params[:id])
  end

  private

  def vehicle_params
    params.permit(:model_id, :year, :mileage, :style, :color)
  end
end
