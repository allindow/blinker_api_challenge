class ApplicationController < ActionController::API
  def handle_errors(object)
    render json: {
      status:  "error",
      code:    400,
      message: object.errors.full_messages.join(", ")
    }, status:   400
  end
end
