class Model < ApplicationRecord
  belongs_to :make, optional: true
  has_many :vehicles
  validates :name, presence: true, uniqueness: { scope: :make_id }
  validate :make_exists

  def make_exists
    if make_id
      errors.add(:make, "not found") unless Make.find_by_id(make_id)
    end
  end
end