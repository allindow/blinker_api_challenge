class Vehicle < ApplicationRecord
  belongs_to :model
  has_many :vehicle_options
  has_many :options, through: :vehicle_options
  validate :available_style, :available_color
  validate :model_exists

  STYLES = ["Sedan", "Minivan", "SUV", "Pickup"]
  COLORS = ["Red", "Blue", "Yellow", "Green", "Black", "Silver", "White"]

  def available_style
    errors.add(:style, "#{style} is not an option") unless STYLES.include?(style)
  end

  def available_color
    errors.add(:color, "#{color} is not an option") unless COLORS.include?(color)
  end

  def model_exists
    if model_id
      errors.add(:model, "not found") unless Model.find_by_id(model_id)
    end
  end
end