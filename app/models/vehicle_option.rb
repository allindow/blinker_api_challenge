class VehicleOption < ApplicationRecord
  belongs_to :vehicle
  belongs_to :option
  validates_presence_of :vehicle
  validates_presence_of :option
  validates :vehicle_id, uniqueness: { scope: :option_id }
end