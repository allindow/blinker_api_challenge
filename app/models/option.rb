class Option < ApplicationRecord
  has_many :vehicle_options
  has_many :vehicles, through: :vehicle_options
  validates :name, presence: true, uniqueness: true
end